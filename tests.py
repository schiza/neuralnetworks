from classes.base import *
from classes.kohonen import *
from classes.grossberg import *

def testXOR():
    print("losowe wagi")
    n = NeuralNetwork(2, [NeuralLayer([Neuron(3, ActivationFunction.Sigmoid),
                                       Neuron(3, ActivationFunction.Sigmoid)]),
                          NeuralLayer([Neuron(3, ActivationFunction.Sigmoid)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n\n")

    print("wagi po uczeniu")
    n = NeuralNetwork(2, [NeuralLayer([Neuron([-2.261652, -6.438204, -6.433465], ActivationFunction.Sigmoid),
                                       Neuron([ 5.796171,  3.752346,  3.751362], ActivationFunction.Sigmoid)]),
                          NeuralLayer([Neuron([-3.885399, -7.697352, -7.924756], ActivationFunction.Sigmoid)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n")
    print(n.evalOutput([0.5, 0.5]), end="\n\n")
    
    

def testANDStep():
    print("losowe wagi")
    n = NeuralNetwork(2, [NeuralLayer([Neuron(3, ActivationFunction.Step)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n\n")

    print("wagi po uczeniu")
    n = NeuralNetwork(2, [NeuralLayer([Neuron([0.6613369, 0.3700303, 0.4520491], ActivationFunction.Step)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n")

def testANDSigmoid():
    print("losowe wagi")
    n = NeuralNetwork(2, [NeuralLayer([Neuron(3, ActivationFunction.Sigmoid)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n\n")

    print("wagi po uczeniu")
    n = NeuralNetwork(2, [NeuralLayer([Neuron([8.93, 5.8929, 5.8927], ActivationFunction.Sigmoid)])])
    print(n.evalOutput([0.01, 0.99]), end="\n\n")
    print(n.evalOutput([0.99, 0.01]), end="\n\n")
    print(n.evalOutput([0.99, 0.99]), end="\n\n")
    print(n.evalOutput([0.01, 0.01]), end="\n\n")
    print(n.evalOutput([0.2, 0.8]), end="\n\n")
    print(n.evalOutput([0.8, 0.3]), end="\n\n")
    print(n.evalOutput([0.8, 0.8]), end="\n\n")



def printWeights(stringArrays, joinSign="\t\t"):
    for i in range(len(stringArrays[0])):
        sb = []
        for array in stringArrays:
            sb.append(array[i])
            
        print(joinSign.join(sb))
        

def weightsToString(neurons, title = ""):
    sb = [title]
    j = 1
    for neuron in neurons:
        sb.append("----- Neuron %d -----" % j)
        for i in range(0, 7, 3):
            sb.append("%.4f %.4f %.4f" % (neuron.weights[i+0], neuron.weights[i+1], neuron.weights[i+2]))
        j += 1
    sb.append("====================")
    return sb
    
def kohonen(testInputs, checkInputs):
    layer = KohonenLayer(neurons = 4, inputSize = 9, minWeight = 0, maxWeight = 0.001)
    network = NeuralNetwork(layers = [layer], bias = -1)
    before = weightsToString(layer.neurons, "Weights before learn")
    kl = KohonenLearning(layer, iterations = 3000, startingRadius = 0, minConscience = 0.75)
    kl.learn(testInputs)
    after = weightsToString(layer.neurons, "Weights after learn")
    printWeights([before, after])
    
    print("Test inputs\n===========")
    j = 1
    testClassifications = []
    for testInput in testInputs:
        print("--- Test input %d ---" % j)
        x = layer.evalOutput(testInput)
        
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (testInput[i+0], testInput[i+1], testInput[i+2])
            if i == 3:
                classification = layer.classification()
                testClassifications.append(classification)
                s += "\t\tClassification: neuron %d" % classification
            print(s)
        j += 1

    print("Check inputs\n===========")
    j = 1
    checkClassifications = []
    for checkInput in checkInputs:
        print("--- Check input %d ---" % j)
        x = layer.evalOutput(checkInput)
            
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (checkInput[i+0], checkInput[i+1], checkInput[i+2])
            if i == 3:
                classification = layer.classification()
                checkClassifications.append(classification)
                s += "\t\tClassification: neuron %d" % classification
            print(s)
        j += 1
    diffs = [x == y for (x,y) in zip(testClassifications, checkClassifications)]
    successRate = (len([b for b in diffs if b == True])*100)/len(diffs)
    print("Success rate: %d%%" % successRate)

    
    
def counterpropagation(testInputs, checkInputs):
    print("Kohonen layer learning\n===========")
    kohonenLayer = KohonenLayer(neurons = 4, inputSize = 9, minWeight = 0, maxWeight = 0.001)
    network = NeuralNetwork(layers = [kohonenLayer], bias = -1)
    before = weightsToString(kohonenLayer.neurons, "Weights before learn")
    kl = KohonenLearning(kohonenLayer, iterations = 3000, startingRadius = 0, minConscience = 0.75)
    kl.learn(testInputs)
    after = weightsToString(kohonenLayer.neurons, "Weights after learn")
    printWeights([before, after])
    
    print("\nGrossberg layer learning\n===========")
    grossbergLayer = GrossbergLayer(neurons = 9, inputSize = 4, minWeight = 0, maxWeight = 1)
    grossbergLearning = GrossbergLearning(grossberg = grossbergLayer, kohonen = kohonenLayer,
        iterations = 1000)
    
    #before = weightsToString(grossbergLayer.neurons, "Weights before learn")
    grossbergLearning.learn(testInputs)
    #after = weightsToString(grossbergLayer.neurons, "Weights before learn")
    #printWeights([before, after])
    
    print("\nTest inputs\n===========")
    j = 1
    for testInput in testInputs:
        print("--- Test input %d ---\t --- Output ---" % j)
        kohonenLayer.evalOutput(testInput)
        classificationKohonen = kohonenLayer.classificationOutput()
        classification = grossbergLayer.classification(classificationKohonen)
        
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (testInput[i+0], testInput[i+1], testInput[i+2])
            s = s + "       \t%0.2f  %0.2f  %0.2f" % (classification[i+0], classification[i+1], classification[i+2])
            print(s)
        j += 1
        

    print("\nCheck inputs\n===========")
    j = 1
    for checkInput in checkInputs:
        print("--- Check input %d ---\t --- Output ---" % j)
        kohonenLayer.evalOutput(checkInput)
        classificationKohonen = kohonenLayer.classificationOutput()
        classification = grossbergLayer.classification(classificationKohonen)
        
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (checkInput[i+0], checkInput[i+1], checkInput[i+2])
            s = s + "        \t%0.2f  %0.2f  %0.2f" % (classification[i+0], classification[i+1], classification[i+2])
            print(s)
        j += 1
    #diffs = [x == y for (x,y) in zip(testClassifications, checkClassifications)]
    #successRate = (len([b for b in diffs if b == True])*100)/len(diffs)
    #print("Success rate: %d%%" % successRate)


    
testInputs = [
    [0, 1, 0,
     1, 1, 1,
     0, 1, 0],
    [1, 1, 1,
     1, 0, 1,
     1, 1, 1],
    [1, 0, 0,
     0, 1, 0,
     0, 0, 1],
    [0, 0, 1,
     0, 1, 0,
     1, 0, 0]
]

checkInputs = [
    [0, 1, 0,
     1, 1, 0,
     0, 1, 0],
    [1, 0, 1,
     1, 0, 1,
     1, 1, 1],
    [0, 0, 0,
     0, 1, 0,
     0, 0, 1],
    [0, 0, 1,
     0, 1, 0,
     0, 0, 0]
 ]

counterpropagation(testInputs, checkInputs)
