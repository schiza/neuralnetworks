from classes.base import *
from classes.backpropagation import *

xorTrain = [
    [[0.0,0.0], [0.0]],
    [[0.0,1.0], [1.0]],
    [[1.0,0.0], [1.0]],
    [[1.0,1.0], [0.0]]
]

def testXOR():
    print("losowe wagi")
    n = NeuralNetwork(2, [NeuralLayer([Neuron(2, ActivationFunction.Sigmoid),
                                       Neuron(2, ActivationFunction.Sigmoid)]),
                          NeuralLayer([Neuron(2, ActivationFunction.Sigmoid)], bias = -1)])
    print("%0.2f\n" % (n.evalOutput([0.01, 0.99])[0]))
    print("%0.2f\n" % (n.evalOutput([0.99, 0.01])[0]))
    print("%0.2f\n" % (n.evalOutput([0.99, 0.99])[0]))
    print("%0.2f\n" % (n.evalOutput([0.01, 0.01])[0]))

    bp = BPLearning(n, iterations = 40000, learningRate = 0.2, momentum = 0.25)
    bp.learn(xorTrain);
    
    print("wagi po uczeniu")
    print("%0.2f\n" % (n.evalOutput([0.01, 0.99])[0]))
    print("%0.2f\n" % (n.evalOutput([0.99, 0.01])[0]))
    print("%0.2f\n" % (n.evalOutput([0.99, 0.99])[0]))
    print("%0.2f\n" % (n.evalOutput([0.01, 0.01])[0]))
    
    totalErr = 0.0
    for example in xorTrain:
        input = example[0]
        trainOut = example[1][0]
        output = n.evalOutput(input)[0]
        totalErr += (output-trainOut)*(output-trainOut);
    print("error = %0.3f\n" % (totalErr))


iconsTrain = [
    [[1, 0, 1,
     0, 1, 0,
     1, 0, 1], [1,0,0]],
    [[1, 1, 1,
     0, 1, 0,
     0, 1, 0], [0,1,0]],
    [[0, 1, 0,
     1, 1, 1,
     0, 1, 0], [0,0,1]]
]

def testIcons():
    print("losowe wagi")
    n = NeuralNetwork(9, [NeuralLayer([Neuron(9, ActivationFunction.Sigmoid),
                        Neuron(9, ActivationFunction.Sigmoid),
                        Neuron(9, ActivationFunction.Sigmoid)], bias = -1)])
    for example in iconsTrain:
        input = example[0]
        output = n.evalOutput(example[0])
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (input[i+0], input[i+1], input[i+2])
            if i == 6:
                s += "\t [%0.3f %0.3f %0.3f]\n" % (output[0], output[1], output[2])
            print(s)

    bp = BPLearning(n, iterations = 5000, learningRate = 0.9, momentum = 0.6)
    bp.learn(iconsTrain);
    
    print("wagi po uczeniu")
    for example in iconsTrain:
        input = example[0]
        output = n.evalOutput(example[0])
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (input[i+0], input[i+1], input[i+2])
            if i == 6:
                s += "\t [%0.3f %0.3f %0.3f]\n" % (output[0], output[1], output[2])
            print(s)


icons2Train = [
    [[1, 1, 1,
     0, 0, 0,
     1, 1, 1], [1,0,0]],
    [[1, 1, 1,
     0, 0, 0,
     0, 0, 0], [0,1,0]],
    [[0, 0, 0,
     0, 0, 0,
     1, 1, 1], [0,0,1]]
]

def testIcons2():
    print("losowe wagi")
    n = NeuralNetwork(9, [NeuralLayer([Neuron(9, ActivationFunction.Sigmoid),
                        Neuron(9, ActivationFunction.Sigmoid),
                        Neuron(9, ActivationFunction.Sigmoid)], bias = -1)])
    for example in icons2Train:
        input = example[0]
        output = n.evalOutput(example[0])
        for i in range(0, 7, 3):
            s = "      %d  %d  %d" % (input[i+0], input[i+1], input[i+2])
            if i == 6:
                s += "\t [%0.3f %0.3f %0.3f]\n" % (output[0], output[1], output[2])
            print(s)

    bp = BPLearning(n, iterations = 25000, learningRate = 0.9, momentum = 0.6)
    bp.learn(icons2Train);
    
    print("wagi po uczeniu")
    for neuron in n.layers[0].neurons:
        for i in range(0, 7, 3):
            print("      %0.2f\t%0.2f\t%0.2f\n" % (neuron.weights[i+0], neuron.weights[i+1], neuron.weights[i+2]))
        print("      %0.2f\n" % (neuron.weights[9]))
        print("\n")
    
    #totalErr = 0
    for example in icons2Train:
        trainOut = example[1]
        output = n.evalOutput(example[0])
        err = functools.reduce(operator.add, [ (x-y)*(x-y) for (x,y) in zip(trainOut, output)])
        print("error %0.5f\n" % math.sqrt((err/3.0)))
    


bitTrain = [
    [[0.0, 0.0, 0.0], [0.0]],
    [[0.0, 0.0, 1.0], [1.0]],
    [[0.0, 1.0, 0.0], [1.0]],
    [[0.0, 1.0, 1.0], [0.0]],
    [[1.0, 0.0, 0.0], [1.0]],
    [[1.0, 0.0, 1.0], [0.0]],
    [[1.0, 1.0, 0.0], [0.0]],
    [[1.0, 1.0, 1.0], [1.0]],
]

def testBitParity():
    #print("losowe wagi")
    n = NeuralNetwork(3, [NeuralLayer([Neuron(2, ActivationFunction.Sigmoid) for i in range(6)]),
                          NeuralLayer([Neuron(6, ActivationFunction.Sigmoid)], bias = -1)])

    bp = BPLearning(n, iterations = 12000, learningRate = 0.2, momentum = 0.5)
    bp.learn(bitTrain);
    
    totalErr = 0.0
    for example in bitTrain:
        input = example[0]
        trainOut = example[1][0]
        output = n.evalOutput(input)[0]
        print((trainOut, output))
        totalErr += (output-trainOut)*(output-trainOut)
    print("error = %0.3f\n" % (totalErr))


#testIcons()
#testXOR()
#testIcons2()
testBitParity()