import math
import sys
from classes.base import *

class NeighbourFactorFunctions:
    @staticmethod
    def SimpleBubble(n1, n2, neuronMap, startingRadius, k):
        if startingRadius == 0 or startingRadius == None:
            return 1 if n1 == n2 else 0
        neuronCount = len(neuronMap)
        currentRadius = startingRadius - k/float(neuronCount)
        neighborhood_distance = abs(neuronMap.index(n1) - neuronMap.index(n2))
        return neighborhood_distance if neighborhood_distance < currentRadius else 0
            
class NeighbourPlacementFunctions:
    @staticmethod
    def Linear(neurons):
        return neurons

class LearningFactorFunctions:
    @staticmethod
    def Linear(l0, t, tmax):
        return l0*(tmax - t)/tmax
        
class DistanceMetrics:
    @staticmethod
    def Euclides(v1, v2):
        return math.sqrt(sum([(x1 - x2)**2 for (x1, x2) in zip(v1,v2)]))

class KohonenLayer(NeuralLayer):
    def classificationOutput(self):
        values = self.lastOutput
        maxIndex = 0
        maxValue = 0
        for value in values:
            if value > maxValue:
                maxIndex = values.index(value)
                maxValue = value
        output = [0 for i in range(len(values))]
        output[maxIndex] = 1
        return output

    def classification(self):
        output = self.classificationOutput()
        return output.index(1) + 1
        
class KohonenLearning(object):
    def __init__(self, kohonenLayer, iterations,
                 startingLearningFactor = 0.5,
                 learningFactorFunction = LearningFactorFunctions.Linear,
                 minConscience = 0.75,
                 startingRadius = 1.99,
                 neighbourFactorFunction = NeighbourFactorFunctions.SimpleBubble,                 
                 neighbourPlacementFunction = NeighbourPlacementFunctions.Linear,
                 distanceFunction = DistanceMetrics.Euclides):
        self.kohonenLayer = kohonenLayer
        self.iterations = iterations
        self.startingLearningFactor = startingLearningFactor
        self.learningFactorFunction = learningFactorFunction
        self.minConscience = minConscience
        self.startingRadius = startingRadius
        self.neighbourFactorFunction = neighbourFactorFunction
        self.neighbourPlacementFunction = neighbourPlacementFunction
        self.distanceFunction = distanceFunction
        #== end attrs rewrite
        self.currentTime = 0
        self.currentWinner = None
        self.conscience = {}
        self.neuronMap = self.neighbourPlacementFunction(self.kohonenLayer.neurons)

    def learn(self, testInputs):
        if self.minConscience == 0:
            self.updateConscience()
        for k in range(self.iterations):
            sys.stdout.write("\r%d/%d iterations (%d%%)" % (k+1, self.iterations, ((k+1)*100)/self.iterations))
            self.currentIteration = k
            self.currentLearningFactor = self.learningFactorFunction(self.startingLearningFactor, self.currentIteration, self.iterations)
            for testInput in testInputs:
                if self.minConscience != 0:
                    self.updateConscience()
                self.currentInput = testInput
                self.kohonenLayer.evalOutput(testInput)
                self.currentWinner = self.chooseWinner()
                self.updateWeights()
        print()

    def updateConscience(self):
        if len(self.conscience) == 0:
            for neuron in self.kohonenLayer.neurons:
                self.conscience[neuron.id] = 1
        else:
            for neuron in self.kohonenLayer.neurons:
                if neuron == self.currentWinner:
                    self.conscience[neuron.id] -= self.minConscience
                else:
                    new_conscience = self.conscience[neuron.id] + 1/len(self.kohonenLayer.neurons)
                    self.conscience[neuron.id] = new_conscience if new_conscience <= 1 else 1 
                
                
    def chooseWinner(self):
        winner = None
        minDistance = None
        for neuron in self.kohonenLayer.neurons:
            if (neuron.id not in self.conscience) or self.conscience[neuron.id] < self.minConscience:
                continue
            distance = self.distanceFunction(self.currentInput, neuron.weights)
            if minDistance == None or distance < minDistance:
                minDistance = distance
                winner = neuron
        return winner

    def updateWeights(self):
        for neuron in self.kohonenLayer.neurons:
            self.updateNeuronWeights(neuron)

    def updateNeuronWeights(self, neuron):
        neighbourhood_factor = self.neighbourFactorFunction(neuron, self.currentWinner, self.neuronMap, self.startingRadius, self.currentIteration)
        if neighbourhood_factor > 0:
            input_difference = [x - w for (x, w) in zip(self.currentInput, neuron.weights)]
            weights_update = [self.currentLearningFactor * neighbourhood_factor * y for y in input_difference]
            neuron.weights = [w + n for (w, n) in zip(neuron.weights, weights_update)]        
