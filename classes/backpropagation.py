import math
import sys
from classes.base import *

class LearningFactorFunctions:
    @staticmethod
    def Linear(l0, t, tmax):
        return l0*(tmax - t)/tmax


class Derivatives:
    @staticmethod
    def Linear():
        return 1.0

    @staticmethod
    def Sigmoid(x):
        ex = math.exp(-x)
        return ex / ((ex+1)*(ex+1))


class BPLearning(object):
    def __init__(self, network, iterations,
                learningRate = 0.1,
                momentum = 0.2):
        self.network = network
        self.iterations = iterations
        self.learningRate = learningRate
        self.momentum = momentum
        
    def learn(self, examples):
        neurons = self.network.neurons()
        delta = [random.random()*0.1 for i in range(len(neurons))]
        print(delta)
        for k in range(self.iterations):
            sys.stdout.write("\r%d/%d iterations (%d%%)" % (k+1, self.iterations, ((k+1)*100)/self.iterations))
            alpha = self.learningRate
            for example in examples:
                input = example[0]
                output = example[1]
                networkOutput = self.network.evalOutput(input)
                i = 0
                for neuron in self.network.layers[-1].neurons:
                    delta[neuron.id-1] = Derivatives.Sigmoid(neuron.lastSum) * (output[i] - neuron.lastOut)
                    i = i+1
                L = len(self.network.layers)
                for l in [L-2-q for q in range(0, L-1)]:
                    layer = self.network.layers[l].neurons
                    i = 0
                    for neuron in layer:
                        delta[neuron.id-1] = 0.0
                        for nn in self.network.layers[l+1].neurons:
                            delta[neuron.id-1] += delta[nn.id-1] * nn.weights[i]
                        delta[neuron.id-1] *= Derivatives.Sigmoid(neuron.lastSum)
                        i += 1
                self.updateWeights(delta, alpha, self.momentum)
        print()
        
    def updateWeights(self, delta, alpha, momentum):
        layers = self.network.layers
        #print("new weights:")
        for layer in layers:
            for n in layer.neurons:
                d = delta[n.id-1]
                for i in range(len(n.weights)-1):
                    a = layer.lastInput[i]
                    n.weights[i] += alpha * a * d + momentum * n.lastChange[i]
                    n.lastChange[i] = alpha * a * d + momentum * n.lastChange[i]
                if layer.bias != None:
                    n.weights[-1] += alpha * layer.bias * d + momentum * n.lastChange[-1]
                #print(n.weights)