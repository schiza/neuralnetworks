import math
import sys
from classes.base import *

class LearningFactorFunctions:
    @staticmethod
    def Linear(l0, t, tmax):
        return l0*(tmax - t)/tmax

class GrossbergLayer(NeuralLayer):
    def classification(self, testInput):
        self.evalOutput(testInput)
        return self.lastOutput
    
    

class GrossbergLearning(object):
    def __init__(self, grossberg, kohonen, iterations,
                learningFactorFunction = LearningFactorFunctions.Linear,
                startingLearningFactor = 0.1):
        self.grossberg = grossberg
        self.kohonen = kohonen
        self.iterations = iterations
        self.learningFactorFunction = learningFactorFunction
        self.startingLearningFactor = startingLearningFactor
        
    def learn(self, testInputs):
        for k in range(self.iterations):
            sys.stdout.write("\r%d/%d iterations (%d%%)" % (k+1, self.iterations, ((k+1)*100)/self.iterations))
            alpha = self.learningFactorFunction(self.startingLearningFactor, k, self.iterations)
            for testInput in testInputs:
                self.kohonen.evalOutput(testInput)
                c = self.kohonen.classification() - 1
                self.grossberg.evalOutput(testInput)
                self.updateWeights(testInput, c, alpha)
        print()
        
    def updateWeights(self, input, c, alpha):
        n = len(self.grossberg.neurons)
        for i in range(n):
            self.grossberg.neurons[i].weights[c] = self.grossberg.neurons[i].weights[c] + alpha * (input[i] - self.grossberg.neurons[i].weights[c])
        pass