import math
import functools
import operator
import random

class ActivationFunction:
    @staticmethod
    def Step(x, threshold = 0):
        if x <= threshold:
            return 0
        else:
            return 1

    @staticmethod
    def Linear(x):
        return x

    @staticmethod
    def Sigmoid(x):        
        return 1.0 / (1.0 + math.exp(-x))

class NeuralNetwork:
    def __init__(self, inputSize = 1, layers = 1, bias = None, defaultActivationFunction = ActivationFunction.Sigmoid, minWeight = 0.0, maxWeight = 1.0):
        # layers jest losowane, jesli podamy int
        if isinstance(layers, int):
            self.layers = [ NeuralLayer(
                [Neuron(inputSize, defaultActivationFunction) for i in range(layers)]
            )]
        else:
            self.layers = layers
        self.bias = bias
        self.lastOutput = None
            
    def evalOutput(self, inputs):
        values = inputs
        for layer in self.layers:
            values = layer.evalOutput(values, self.bias)
        self.lastOutput = values
        return self.lastOutput
        

    def neurons(self):
        n = []
        for layer in self.layers:
            n += layer.neurons
        return n

class NeuralLayer:
    def __init__(self, neurons, inputSize = None, bias = 0, minWeight = 0, maxWeight = 1.0):
        if isinstance(neurons, int):
            self.neurons = [Neuron(weights = inputSize, bias = bias, weightMin = minWeight, weightMax = maxWeight) for i in range(neurons)]
        else:
            self.neurons = neurons
        self.bias = bias
        self.lastOutput = None
        
    def evalOutput(self, inputs, bias = 0):
        b = bias if bias != None else self.bias
        self.lastInput = inputs
        self.lastOutput = [neuron.evalOutput(inputs, b) for neuron in self.neurons]
        return self.lastOutput

#class OutputLayer:

class Neuron:
    lastId = 0
    
    def __init__(self, weights, activationFunction = ActivationFunction.Linear, weightMin = 0.0, weightMax = 1.0, bias = -1):
        # weights moze byc wektorem (arrayem) wag lub int'em (wtedy losujemy tyle wag)
        if isinstance(weights, int):
            self.weights = [random.random() * (weightMax - weightMin) + weightMin for i in range(weights+1)]
        else:
            self.weights = weights
        self.lastChange = [0 for i in range(len(self.weights))]
        self.activationFunction = activationFunction
        Neuron.lastId += 1
        self.id = Neuron.lastId
        self.bias = bias
            
    def evalOutput(self, inputs, bias = None):
        if bias != None:
            b = bias
        else:
            b = self.bias if self.bias != None else 0        
        #sum += b
        sum = functools.reduce(operator.add, [x*w for (x,w) in zip(inputs+[b], self.weights)])
        self.lastSum = sum
        self.lastOut = self.activationFunction(sum)
        return self.activationFunction(sum)
        
    def setWeights(self, weights):
        self.weights = weights
